﻿using System;
using System.Collections.Generic;

namespace PautaExamen1
{
    class Program
    {
        static void Main(string[] args)
        {
            var vacio = new int[]{ };
            var A1 = new int[] { 1, 2, 3, -1, -10, -20 };
            var A2 = new int[] { 1, 2, 3 };
            var B1 = new int[] { 6 };
            var B2 = new int[] { 1, 3, 6 };
            var B3 = new int[] { 1, 2, 3, 4, 6 };
            var C1 = new int[] { 34, 56, 11, 13, 22 };
            Console.WriteLine("Primer ejercicio");
            ImprimirArray(SumaArregloPositivosNegativos(null));
            ImprimirArray(SumaArregloPositivosNegativos(vacio));
            ImprimirArray(SumaArregloPositivosNegativos(A1));
            ImprimirArray(SumaArregloPositivosNegativos(A2));

            Console.WriteLine("Segudo ejercicio");
            ImprimirArray(SecuenciaFaltante(null));
            ImprimirArray(SecuenciaFaltante(vacio));
            ImprimirArray(SecuenciaFaltante(B1));
            ImprimirArray(SecuenciaFaltante(B2));
            ImprimirArray(SecuenciaFaltante(B3));

            Console.WriteLine("Tercer ejercicio");
            ImprimirArray(CercanoACinco(null));
            ImprimirArray(CercanoACinco(vacio));
            ImprimirArray(CercanoACinco(C1));

            Console.ReadKey();
        }

        static int[] SumaArregloPositivosNegativos(int[] A)
        {
            int positivos, negativos;

            if (ValidarArreglosInvalidos(A))
            {
                return new int[] { };
            }

            positivos = 0;
            negativos = 0;

            foreach (int elemento in A)
            {
                if (elemento < 0)
                {
                    negativos += elemento;
                }
                else
                {
                    positivos += elemento;
                }
            }

            var resultado = new int[] { positivos, negativos };
            return resultado;
        }

        static int[] SecuenciaFaltante(int[] entrada)
        {
            if (ValidarArreglosInvalidos(entrada))
            {
                return new int[] { };
            }

            // Asumiendo que esta ordenado el arreglo
            int contador = 1;
            List<int> faltantes = new List<int>();

            for(int i = 0; i < entrada.Length; i++)
            {
                int valor = entrada[i];

                while (contador < valor)
                {
                    faltantes.Add(contador++);
                }

                if (contador == valor)
                {
                    contador++;
                }
            }

            return faltantes.ToArray();
        }

        static int[] CercanoACinco(int[] numeros)
        {
            if (ValidarArreglosInvalidos(numeros))
            {
                return new int[] { };
            }

            var tamaño = numeros.Length;
            var respuesta = new int[tamaño];

            for (int i = 0; i < tamaño; i++)
            {
                int residuo = numeros[i] % 5;

                if (residuo == 0)
                {
                    respuesta[i] = numeros[i];
                }
                else
                {
                    if (residuo < 2.5)
                    {
                        respuesta[i] = numeros[i] - residuo;
                    }
                    else
                    {
                        respuesta[i] = numeros[i] + 5 - residuo;
                    }
                }
            }
            

            return respuesta;
        }

        static bool ValidarArreglosInvalidos(int[] arreglo)
        {
            if (arreglo == null)
            {
                return true;
            }

            if (arreglo.Length == 0)
            {
                return true;
            }

            return false;
        }


        static void ImprimirArray(int[] arreglo)
        {
            Console.Write("{");

            for (int i = 0; i < arreglo.Length; i++)
            {
                if (i > 0)
                {
                    Console.Write(", ");
                }

                Console.Write(arreglo[i]);
            }

            Console.WriteLine("}");
        }
    }
}
